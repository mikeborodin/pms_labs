package io.github.mikeborodin.multimedia

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup.LayoutParams
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import io.github.mikeborodin.multimedia.DrawView.Caption
import io.github.mikeborodin.multimedia.opengl.lab1.Hexagon
import io.github.mikeborodin.multimedia.opengl.lab1.Pentagon
import io.github.mikeborodin.multimedia.opengl.lab1.PolySurfaceView
import io.github.mikeborodin.multimedia.opengl.lab1.Square
import io.github.mikeborodin.multimedia.opengl.lab1.Triangle
import kotlinx.android.synthetic.main.activity_main.aMinusBtn
import kotlinx.android.synthetic.main.activity_main.aPlusBtn
import kotlinx.android.synthetic.main.activity_main.addBtn
import kotlinx.android.synthetic.main.activity_main.kMinus
import kotlinx.android.synthetic.main.activity_main.kPlus
import kotlinx.android.synthetic.main.activity_main.msg
import kotlinx.android.synthetic.main.activity_main.removeBtn
import kotlinx.android.synthetic.main.activity_main.root
import java.util.Random

class MainActivity : AppCompatActivity() {

    lateinit var glView: PolySurfaceView
    lateinit var drawView: DrawView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        glView = PolySurfaceView(this)
        glView.layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)

        drawView = DrawView(this).apply {
            bringToFront()
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
        }

        root.addView(glView)
//        root.addView(drawView)


        addBtn.bringToFront()
        removeBtn.bringToFront()
        msg.bringToFront()

        addBtn.setOnClickListener {
            glView.queueEvent {
                glView.mRenderer.objects.add(Square().apply {
                    offsetX = x
                    offsetY = y
                    scale = .1f
                })
            }
            animateSquare(true)
        }

        removeBtn.setOnClickListener {
            glView.queueEvent {
                glView.mRenderer.objects.clear()

            }
        }

        aPlusBtn.setOnClickListener {
            a += .1f
            msg.text = "f(x)=${a.toString().substring(0, 3)}*x^2; K=${k}"
        }

        aMinusBtn.setOnClickListener {
            a -= .1f
            msg.text = "f(x)=${a.toString().substring(0, 3)}*x^2; K=${k}"
        }

        kMinus.setOnClickListener {
            k -= 1f
        }

        kPlus.setOnClickListener {
            k -= 1f
        }


        msg.text = "f(x)=${a.toString().substring(0, 3)}*x^2; K=${k}"

    }

    var k = 1f
    val x = 0f
    val y = 0f
    var a = 1f

    fun f(x: Float) = a * Math.pow(x.toDouble(), 2.0).toFloat()

    private var created: Boolean = false

    fun animateSquare(right: Boolean) {


        Handler().apply {
            for (i in -200..100) {
                postDelayed({
                    glView.queueEvent {
                        Log.d("posX", "${-i / 10f}")
                        val x = 1 - i / 50f

                        glView.mRenderer.objects[0].scale += .001f + k / 10000//* if (i > 15) -1 else 1
                        glView.mRenderer.objects[0].offsetX = x * if (right) -1 else 1
                        glView.mRenderer.objects[0].offsetY = f(x)
                    }
                }, 5L * i)
            }
            postDelayed({
                glView.queueEvent {
                    val oldX = glView.mRenderer.objects[0].offsetX
                    val oldY = glView.mRenderer.objects[0].offsetY
                    val oldScale = glView.mRenderer.objects[0].scale / 5 * k/10000

                    glView.mRenderer.objects.clear()
                    glView.mRenderer.objects.add(Square().apply {
                        offsetX = oldX
                        offsetY = oldY
                        scale = oldScale
                    })

                }
                animateSquare(!right)
            }, 50L * 19)
        }
    }


    fun addObj() {

        val sides = Random().nextInt(4)

        var x = Random().nextFloat() * 1.2f //Random().nextInt(100) / 10f
        if (Random().nextBoolean()) x *= -1
        var y = Random().nextFloat() * 1.2f//Random().nextInt(100) / 10f
        if (Random().nextBoolean()) y *= -1
        glView.queueEvent {
            when (sides) {
                3 -> {
                    glView.mRenderer.objects.add(Triangle().apply {
                        offsetX = x
                        offsetY = y
                    }
                    )
                }
                4 -> {
                    glView.mRenderer.objects.add(Square().apply {
                        offsetX = x
                        offsetY = y
                    }
                    )
                }
                5 -> {
                    glView.mRenderer.objects.add(Pentagon().apply {
                        offsetX = x
                        offsetY = y
                    }
                    )
                }
                6 -> {
                    glView.mRenderer.objects.add(Hexagon().apply {
                        offsetX = x
                        offsetY = y
                    }
                    )
                }
            }
        }

        when (sides) {
            3 -> {
                drawView.captions.add(Caption("Triangle", x, y))
            }
            4 -> {
                drawView.captions.add(Caption("Square", x, y))
            }
            5 -> {
                drawView.captions.add(Caption("Pentagon", x, y))
            }
            6 -> {
                drawView.captions.add(Caption("Hexagon", x, y))
            }
        }
        drawView?.invalidate()

    }

    fun deleteObj() {
        drawView?.captions.clear()
        drawView?.invalidate()
        (1 until glView.mRenderer.objects.size).forEach {
            Handler().apply {
                postDelayed({
                    glView.queueEvent {
                        glView.mRenderer.objects.removeAt(glView.mRenderer.objects.size - 1)
                    }
                }, it * 500L)
            }
        }
    }

    override fun onPause() {
        glView?.onPause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        glView?.onResume()
    }


}
