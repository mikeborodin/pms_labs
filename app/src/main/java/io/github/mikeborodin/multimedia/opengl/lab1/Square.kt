/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.mikeborodin.multimedia.opengl.lab1


/**
 * A two-dimensional square for use as a drawn object in OpenGL ES 2.0.
 */
class Square : Polygon() {

    override fun setup() {
        color = floatArrayOf(
                Math.random().toFloat(),
                Math.random().toFloat(),
                Math.random().toFloat(), 0.0f)
        coords = floatArrayOf(
                -0.5f, 0.5f, 0.0f, // top left
                -0.5f, -0.5f, 0.0f, // bottom left
                0.5f, -0.5f, 0.0f, // bottom right
                0.5f, 0.5f, 0.0f) // top right

        super.setup()

    }
}