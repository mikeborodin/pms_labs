/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License",
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.github.mikeborodin.multimedia.opengl.lab1

import android.opengl.GLUtils

/**
 * A two-dimensional triangle for use as a drawn object in OpenGL ES 2.0.
 */
class Hexagon : Polygon() {

    override fun setup() {
        color = floatArrayOf(0f, 1f, 1.0f, 0.0f)
        coords = floatArrayOf(

                .24f, -.5f, 0f,
                -.24f, -.5f, 0f,
                -.5f, .0f, 0f,

                -.24f, .5f, 0f,
                .24f, .5f, 0f,
                .5f, .0f, 0f


                /*.87f, .50f, 0f,
                .0f, .100f, 0f,
                -.87f, .50f, 0f,
                -.87f, -.50f, 0f,
                .0f, -.100f, 0f,
                .87f, -.50f, 0f*/

//                .600f, .463f, 0f,
//                .500f, .463f, 0f,
//                .450f, .550f, 0f,
//                .500f, .637f, 0f,
//                .600f, .637f, 0f,
//                .650f, .550f, 0f
//                -.21f, -.4f, 0f,
//                -.4f, -.0f, 0f,
//                -.21f, .4f, 0f,
//
//                .21f, .4f, 0f,
//                .4f, .0f, 0f,
//                .21f, -.4f, 0f
        )

        /*coords = FloatArray(sides * 3, { i -> 0f })

        val radius = 1f / (2 * Math.sin(Math.PI / sides))
        var index = 0
        (0..6).forEach { i ->
            coords!![index] = (0f + radius * Math.cos(Math.PI / sides * (1 + 2 * i))).toFloat()
            index++
            coords!![index] = (0f + radius * Math.sin(Math.PI / sides * (1 + 2 * i))).toFloat()
            index++
            coords!![index] = 0f
            index++
        }*/

        super.setup()

    }

}
