package io.github.mikeborodin.multimedia

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.View
import android.widget.Toast


class DrawView(context: Context) : View(context) {

    data class Caption(val text: String, var x: Float, var y: Float)

    val captions: ArrayList<Caption> = arrayListOf()

    private var paint = Paint()

    init {
        paint.color = Color.BLACK
        paint.textSize = 48f
    }

    override fun onDraw(canvas: Canvas?) {
        captions.forEach {
            drawPoly(it, canvas)
        }
    }

    private fun drawPoly(it: Caption, canvas: Canvas?) {
        val x  = width / 2f-it.x*500
        val y = height / 2f-it.y*500
        canvas?.drawText(it.text, x, y, paint)
    }


}