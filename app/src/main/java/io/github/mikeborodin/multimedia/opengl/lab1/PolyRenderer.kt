/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.mikeborodin.multimedia.opengl.lab1

import android.opengl.GLES20
import android.opengl.GLSurfaceView.Renderer
import android.opengl.Matrix
import android.util.Log
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

/**
 * Provides drawing instructions for a PolySurfaceView object. This class
 * must override the OpenGL ES drawing lifecycle methods:
 *
 */
class PolyRenderer : Renderer {

    val objects: ArrayList<Polygon> = arrayListOf()

    // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
    private val mMVPMatrix = FloatArray(16)
    private val mProjectionMatrix = FloatArray(16)
    private val mViewMatrix = FloatArray(16)
    private val mRotationMatrix = FloatArray(16)

    /**
     * Returns the rotation scaleFactor of the triangle shape (mTriangle).
     *
     * @return - A float representing the rotation scaleFactor.
     */
    /**
     * Sets the rotation scaleFactor of the triangle shape (mTriangle).
     */
    var scaleFactor: Float = 0f

    override fun onSurfaceCreated(unused: GL10, config: EGLConfig) {

        // Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 1.0f, .0f)
    }


    override fun onDrawFrame(unused: GL10) {

        // Draw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)

        // Set the camera position (View matrix)
        Matrix.setLookAtM(mViewMatrix, 0, 0f, 0f, -3f, 0f, 0f, 0f, 0f, 1.0f, 0.0f)

        // Calculate the projection and view transformation
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0)

        Matrix.scaleM(mMVPMatrix, 0, mMVPMatrix, 0, .4f, .4f, 1f)

        //Matrix.scaleM(mMVPMatrix, 0, mMVPMatrix, 0, scaleFactor, scaleFactor, scaleFactor)
        Matrix.setRotateM(mRotationMatrix, 0, scaleFactor, 0f, 0f, 1.0f)
        val scratch = FloatArray(16)
        // Combine the rotation matrix with the projection and camera view
        // Note that the mMVPMatrix factor *must be first* in order
        // for the matrix multiplication product to be correct.
        Matrix.multiplyMM(scratch, 0, mMVPMatrix, 0, mRotationMatrix, 0)

        objects.forEach {
            it.draw(scratch.clone())
        }
    }

    override fun onSurfaceChanged(unused: GL10, width: Int, height: Int) {

        GLES20.glViewport(0, 0, width, height)
        val ratio = width.toFloat() / height

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1f, 1f, 3f, 7f)
    }

    companion object {

        private val TAG = "PolyRenderer"

        /**
         * Utility method for compiling a OpenGL shader.
         *
         * **Note:** When developing shaders, use the checkGlError()
         * method to debug shader coding errors.
         *
         * @param type - Vertex or fragment shader type.
         * @param shaderCode - String containing the shader code.
         * @return - Returns an id for the shader.
         */
        fun loadShader(type: Int, shaderCode: String): Int {

            // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
            // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
            val shaderHandle = GLES20.glCreateShader(type)

            // add the source code to the shader and compile it
            GLES20.glShaderSource(shaderHandle, shaderCode)
            GLES20.glCompileShader(shaderHandle)

            return shaderHandle
        }

        /**
         * Utility method for debugging OpenGL calls. Provide the name of the call
         * just after making it:
         * <pre>
         * mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
         * PolyRenderer.checkGlError("glGetUniformLocation");</pre>
         *
         * If the operation is not successful, the check throws an error.
         *
         * @param glOperation - Name of the OpenGL call to check.
         */
        fun checkGlError(glOperation: String) {
            while (GLES20.glGetError() != GLES20.GL_NO_ERROR) {
                Log.e(TAG, "$glOperation: glError ")
                throw RuntimeException("$glOperation: glError")
            }
        }
    }

}