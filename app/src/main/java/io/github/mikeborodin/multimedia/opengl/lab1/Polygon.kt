package io.github.mikeborodin.multimedia.opengl.lab1

import android.opengl.GLES20
import android.opengl.Matrix
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer


/**
 * Created by mike on 3/11/18.
 * Represent multu gone shape
 */
abstract class Polygon {

    private val vertexShaderCode = "uniform mat4 uMVPMatrix;" +
            "attribute vec4 vPosition;" +
            "void main() {" +
            // the matrix must be included as a modifier of gl_Position
            // Note that the uMVPMatrix factor *must be first* in order
            // for the matrix multiplication product to be correct.
            "  gl_Position = uMVPMatrix * vPosition;" +
            "}"

    private val fragmentShaderCode = "precision mediump float;" +
            "uniform vec4 vColor;" +
            "void main() {" +
            "  gl_FragColor = vColor;" +
            "}"

    var coords: FloatArray? = null
    var color: FloatArray? = null
    var offsetX = 0f
    var offsetY = 0f

    private var vertexBuffer: FloatBuffer? = null
    private var mProgram: Int = 0
    private var mPositionHandle: Int = 0
    private var mColorHandle: Int = 0
    private var mMVPMatrixHandle: Int = 0
    private var vertexCount: Int = 0
    private val vertexStride = COORDS_PER_VERTEX * 4 // 4 bytes per vertex
    var scale = 1f

    open fun setup() {

        vertexCount = coords!!.size / COORDS_PER_VERTEX
        // initialize vertex byte buffer for shape coordinates
        val bb = ByteBuffer.allocateDirect(
                // (number of coordinate values * 4 bytes per float)
                coords!!.size * 4)
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder())

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer()
        // add the coordinates to the FloatBuffer
        vertexBuffer?.put(coords)
        // set the buffer to read the first coordinate
        vertexBuffer?.position(0)
        // prepare shaders and OpenGL program
        val vertexShader = PolyRenderer.loadShader(
                GLES20.GL_VERTEX_SHADER, vertexShaderCode)
        val fragmentShader = PolyRenderer.loadShader(
                GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode)

        mProgram = GLES20.glCreateProgram()             // create empty OpenGL Program
        GLES20.glAttachShader(mProgram, vertexShader)   // add the vertex shader to program
        GLES20.glAttachShader(mProgram, fragmentShader) // add the fragment shader to program
        GLES20.glLinkProgram(mProgram)
    }

    init {
        setup()             // create OpenGL program executables
    }


    fun draw(mvpMatrix: FloatArray) {

        Matrix.translateM(mvpMatrix, 0, mvpMatrix, 0, offsetX, offsetY, 0f)
        Matrix.scaleM(mvpMatrix, 0, mvpMatrix, 0, scale, scale, 0f)

        GLES20.glUseProgram(mProgram)

        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition")

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle)

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(
                mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer)

        // get handle to fragment shader's vColor member
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor")

        // Set color for drawing the triangle
        GLES20.glUniform4fv(mColorHandle, 1, color, 0)

        // get handle to shape's transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix")
        PolyRenderer.checkGlError("glGetUniformLocation")

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0)
        PolyRenderer.checkGlError("glUniformMatrix4fv")

        // Draw the triangle
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, vertexCount)

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle)

    }


    companion object {
        internal val COORDS_PER_VERTEX = 3
    }

}